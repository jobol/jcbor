/*******************************************************************************************************/
/*******************************************************************************************************/
/*************************************                          ****************************************/
/*************************************       T  E  S  T         ****************************************/
/*************************************                          ****************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/

#include <stdio.h>
#include <string.h>

#include "jcbor.h"

void test_string(int r, jcbor_t *x, const char *data, size_t length)
{
	if (r)
		printf("error string build\n");
	else {
		printf("string %.*s\n", (int)jcbor_string_length(x), jcbor_string_value(x));
		if (length != jcbor_string_length(x))
			printf("error string length\n");
		if (strcmp(jcbor_string_value(x), data))
			printf("error string content\n");
		jcbor_unref(x);
	}
}


void test_string_l(void *build, const char *data, size_t length)
{
	jcbor_t *x;
	int r;

	r = ((int (*)(jcbor_t**,const char*,size_t))build)(&x, data, length);
	test_string(r, x, data, length);
}

void test_string_z(void *build, const char *data)
{
	jcbor_t *x;
	int r;

	r = ((int (*)(jcbor_t**,const char*))build)(&x, data);
	test_string(r, x, data, strlen(data));
}

void test_binary(void *build, const void *data, size_t length)
{
	jcbor_t *x;
	int r;

	r = ((int (*)(jcbor_t**,const void*,size_t))build)(&x, data, length);
	if (r)
		printf("error binary build\n");
	else {
		printf("binary %.*s\n", (int)jcbor_binary_length(x), jcbor_binary_value(x));
		if (length != jcbor_binary_length(x))
			printf("error binary length\n");
		if (memcmp(jcbor_binary_value(x), data, length))
			printf("error binary content\n");
		jcbor_unref(x);
	}
}

void test_buffer(const char *str)
{
	size_t len;

	len = strlen(str);
	test_string_z(jcbor_string_copy_z, str);
	test_string_z(jcbor_string_static_z, str);
	test_string_z(jcbor_string_acquire_z, strdup(str));
	test_string_l(jcbor_string_copy, str, len);
	test_string_l(jcbor_string_static, str, len);
	test_string_l(jcbor_string_acquire, strdup(str), len);
	test_binary(jcbor_binary_copy, str, len);
	test_binary(jcbor_binary_static, str, len);
	test_binary(jcbor_binary_acquire, strdup(str), len);
}

int main(int ac, char **av)
{
	test_buffer("Hello world!");

}

