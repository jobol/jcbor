
#include <limits.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "jcbor.h"

#if 1
#	include <assert.h>
#	define ASSERT(x) assert(x)
#else
#	define ASSERT(x) ((void)0)
#endif

#define OFFSETOF(type,field) ((size_t)(&((type*)0)->field))

/*****************************************************************************/
/*****************************************************************************/
/**************                                                 **************/
/**************      internal structures                        **************/
/**************                                                 **************/
/*****************************************************************************/
/*****************************************************************************/

struct _basic32_
{
	uintptr_t flags;
	union {
		uint8_t	u8;
		int32_t s32;
		float f32;
	};
};

struct _basic64_
{
	uintptr_t flags;
	union {
		int64_t s64;
		double f64;
	};
};

union _buffer_
{
	struct {
		uintptr_t flags;
		uint32_t length;
		char value[1];
	};
	struct {
		uintptr_t flags2;
		uint32_t length2;
		union {
			char *str;
			const char *cstr;
		};
	};
};

struct _array_
{
	uintptr_t flags;
	uint32_t count;
	uint32_t alloc;
	jcbor_t **array;
};

struct _reader_
{
	uintptr_t flags;
	uint32_t offset;
	union _buffer_ buffer;
};

/**
 * Describe a jcbor value
 */
union jcbor
{
	uintptr_t flags;
	struct _basic32_  basic32;
	struct _basic64_  basic64;
	union  _buffer_ buffer;
	struct _array_  array;
	struct _reader_ reader;
};


/*****************************************************************************/
/*****************************************************************************/
/**************                                                 **************/
/**************      flags                                      **************/
/**************                                                 **************/
/*****************************************************************************/
/*****************************************************************************/

/*
 * The flag is organized using the following bit definition
 *
 * bits  : len  :  content
 * ------+------+----------
 * 0..4  : 5    :  CborMin (from CBOR)
 * 5..7  : 3    :  CborMaj (from CBOR)
 * 0..7  : 8    :  CborIni (shortcut to inital CBOR type CborMaj+CborMin)0
 * 8..13 : 6    :  Tag + 1
 * 15    : 1    :  Sealed
 * 16    : 1    :  StrVal
 * 17    : 1    :  Free
 * 18..  : N-18 :  RefCount (for 32bits: 14 bits remain 1 ... 16385)
 */
#define CborMin_Shift	0
#define CborMin_Length	5
#define CborMin_Mask	((1 << CborMin_Length) - 1)

#define CborMaj_Shift	(CborMin_Shift + CborMin_Length)
#define CborMaj_Length	3
#define CborMaj_Mask	((1 << CborMaj_Length) - 1)

#define CborIni_Shift	CborMin_Shift
#define CborIni_Length	(CborMin_Length + CborMaj_Length)
#define CborIni_Mask	((1 << CborIni_Length) - 1)

#define Tag_Shift	(CborIni_Shift + CborIni_Length)
#define Tag_Length	6
#define Tag_Mask	((1 << Tag_Length) - 1)

#define Sealed_Shift	(Tag_Shift + Tag_Length)
#define Sealed_Length	1
#define Sealed_Mask	((1 << Sealed_Length) - 1)

#define StrVal_Shift	(Sealed_Shift + Sealed_Length)
#define StrVal_Length	1
#define StrVal_Mask	((1 << StrVal_Length) - 1)

#define Free_Shift	(StrVal_Shift + StrVal_Length)
#define Free_Length	1
#define Free_Mask	((1 << Free_Length) - 1)

#define RefCount_Shift	(Free_Shift + Free_Length)

static inline uintptr_t shifted(int value, int shift)
	{ return (((uintptr_t)value)<<shift); }

static inline uintptr_t set_flags(uintptr_t x, int value, int mask, int shift)
	{ return (x & ~shifted(mask,shift)) | shifted(value,shift); }

static inline uintptr_t get_flags(uintptr_t x, int mask, int shift)
	{ return (x >> shift) & mask; }


static inline uint8_t get_cbor_initial(uintptr_t x)
	{ return (uint8_t)get_flags(x, CborIni_Mask, CborIni_Shift); }

static inline uintptr_t set_cbor_initial(uintptr_t x, uint8_t initial)
	{ return set_flags(x, initial, CborIni_Mask, CborIni_Shift); }

static inline uint8_t make_cbor_initial(uint8_t major, uint8_t minor)
	{ return (major << 5) | minor; }


static inline uint8_t get_cbor_major(uintptr_t x)
	{ return (uint8_t)get_flags(x, CborMaj_Mask, CborMaj_Shift); }

static inline uint8_t get_cbor_minor(uintptr_t x)
	{ return (uint8_t)get_flags(x, CborMin_Mask, CborMin_Shift); }

static inline uintptr_t set_major_minor(uintptr_t x, uint8_t major, uint8_t minor)
	{ return (x & ~(shifted(CborIni_Mask,CborIni_Shift)))
		   | shifted(major,CborMaj_Shift) | shifted(minor,CborMin_Shift); }


static inline bool is_tagged(uintptr_t x)
	{ return !!get_flags(x, Tag_Mask, Tag_Shift); }

static inline uint8_t get_tag(uintptr_t x)
	{ return (uint8_t)(get_flags(x, Tag_Mask, Tag_Shift) - 1); }

static inline uintptr_t set_tag(uintptr_t x, uint8_t tag)
	{ return set_flags(x, 1 + tag, Tag_Mask, Tag_Shift); }

static inline uintptr_t clear_tag(uintptr_t x)
	{ return x & ~shifted(Tag_Mask, Tag_Shift); }

/*****************************************************************************/
/*****************************************************************************/
/**************                                                 **************/
/**************      fake pointers                              **************/
/**************                                                 **************/
/*****************************************************************************/
/*****************************************************************************/

#define Pointer_Fake_Mask	3
#define Pointer_Fake_Special	1
#define Pointer_Fake_Int	2
#define Pointer_Fake_Float	3
#define Pointer_Fake_Shift	2

static inline bool can_fake_value(intmax_t m)
	{ intptr_t x = (intptr_t)m;
	  return (intmax_t)((x << Pointer_Fake_Shift) >> Pointer_Fake_Shift) == m; }

static inline intptr_t fake_value(const jcbor_t *j)
	{ return ((intptr_t)j) >> Pointer_Fake_Shift; }

static inline intptr_t fake_type(const jcbor_t *j)
	{ return ((intptr_t)j) & Pointer_Fake_Mask; }

static inline bool is_regular(const jcbor_t *j)
	{ return fake_type(j) == 0; }

static inline bool is_fake(const jcbor_t *j)
	{ return fake_type(j) != 0; }

static inline jcbor_t *fake_make(intptr_t type, intptr_t value)
	{ return (jcbor_t*)((value << Pointer_Fake_Shift) | type); }


static inline bool is_fake_special(const jcbor_t *j)
	{ return fake_type(j) == Pointer_Fake_Special; }

static inline jcbor_t *fake_special_make(intptr_t value)
	{ return fake_make(Pointer_Fake_Special, value); }

static inline intptr_t fake_special_value(const jcbor_t *j)
	{ return fake_value(j); }


static inline bool can_fake_int(intmax_t value)
	{ return can_fake_value(value); }

static inline bool is_fake_int(const jcbor_t *j)
	{ return fake_type(j) == Pointer_Fake_Int; }

static inline jcbor_t *fake_int_make(intptr_t value)
	{ return fake_make(Pointer_Fake_Int, value); }

static inline intptr_t fake_int_value(const jcbor_t *j)
	{ return fake_value(j); }


#if (defined(INTPTR_WIDTH) && INTPTR_WIDTH == 64) || (defined(INTPTR_MAX) && INTPTR_MAX == 9223372036854775807L)
union fi { float f; intptr_t i; };
static inline bool can_fake_float(float value)
	{ return true; }

static inline bool is_fake_float(const jcbor_t *j)
	{ return fake_type(j) == Pointer_Fake_Float; }

static inline jcbor_t *fake_float_make(float value)
	{ union fi fi; fi.f = value; return fake_make(Pointer_Fake_Float, fi.i); }

static inline float fake_float_value(const jcbor_t *j)
	{ union fi fi; fi.i = fake_value(j); return fi.f; }
#else
static inline bool can_fake_float(float value)
	{ return false; }

static inline bool is_fake_float(const jcbor_t *j)
	{ return false; }

static inline jcbor_t *fake_float_make(float value)
	{ ASSERT(0); return NULL; }

static inline float fake_float_value(const jcbor_t *j)
	{ ASSERT(0); return 0.0f; }
#endif

/*****************************************************************************/
/*****************************************************************************/
/**************                                                 **************/
/**************      CBOR                                       **************/
/**************                                                 **************/
/*****************************************************************************/
/*****************************************************************************/

/*
 * CBOR Major Values
 */

#define CborMaj_PosInt	0
#define CborMaj_NegInt	1
#define CborMaj_Binary	2
#define CborMaj_String	3
#define CborMaj_Array	4
#define CborMaj_Map	5
#define CborMaj_Tag	6
#define CborMaj_Simple	7

/*
 * CBOR Minor Values
 */

#define CborMin_Lower	0
#define CborMin_Higher	23

#define CborMin_False	20
#define CborMin_True	21
#define CborMin_Null	22
#define CborMin_Undefined	23

#define CborMin_1_Byte	24
#define CborMin_2_Bytes	25
#define CborMin_4_Bytes	26
#define CborMin_8_Bytes	27

#define CborMin_Break	31
#define CborMin_ILength	31

#if 0
   | 0            | UTF-8 string     | Standard date/time string; see  |
   |              |                  | Section 2.4.1                   |
   |              |                  |                                 |
   | 1            | multiple         | Epoch-based date/time; see      |
   |              |                  | Section 2.4.1                   |
   |              |                  |                                 |
   | 2            | byte string      | Positive bignum; see Section    |
   |              |                  | 2.4.2                           |
   |              |                  |                                 |
   | 3            | byte string      | Negative bignum; see Section    |
   |              |                  | 2.4.2                           |
   |              |                  |                                 |
   | 4            | array            | Decimal fraction; see Section   |
   |              |                  | 2.4.3                           |
   |              |                  |                                 |
   | 5            | array            | Bigfloat; see Section 2.4.3     |
   |              |                  |                                 |
   | 21           | multiple         | Expected conversion to          |
   |              |                  | base64url encoding; see         |
   |              |                  | Section 2.4.4.2                 |
   |              |                  |                                 |
   | 22           | multiple         | Expected conversion to base64   |
   |              |                  | encoding; see Section 2.4.4.2   |
   |              |                  |                                 |
   | 23           | multiple         | Expected conversion to base16   |
   |              |                  | encoding; see Section 2.4.4.2   |
   |              |                  |                                 |
   | 24           | byte string      | Encoded CBOR data item; see     |
   |              |                  | Section 2.4.4.1                 |
   |              |                  |                                 |
   | 32           | UTF-8 string     | URI; see Section 2.4.4.3        |
   |              |                  |                                 |
   | 33           | UTF-8 string     | base64url; see Section 2.4.4.3  |
   |              |                  |                                 |
   | 34           | UTF-8 string     | base64; see Section 2.4.4.3     |
   |              |                  |                                 |
   | 35           | UTF-8 string     | Regular expression; see         |
   |              |                  | Section 2.4.4.3                 |
   |              |                  |                                 |
   | 36           | UTF-8 string     | MIME message; see Section       |
   |              |                  | 2.4.4.3                         |
   |              |                  |                                 |
   | 55799        | multiple         | Self-describe CBOR; see         |
   |              |                  | Section 2.4.5                   |
#endif

#define CborTag_None		0
#define CborTag_StdDate		1 /* 0 */
#define CborTag_EpochDate	2 /* 1 */
#define CborTag_PosBigNum	3 /* 2 */
#define CborTag_NegBigNum	4 /* 3 */
#define CborTag_DecimalFrac	5 /* 4 */
#define CborTag_Bigfloat	6 /* 5 */
#define CborTag_ToBase64Url	7 /* 21 */
#define CborTag_ToBase64	8 /* 22 */
#define CborTag_ToBase16	9 /* 23 */
#define CborTag_CborData	10 /* 24 */
#define CborTag_Uri		11 /* 32 */
#define CborTag_Base64Url	12 /* 33 */
#define CborTag_Base64		13 /* 34 */
#define CborTag_RegExpr		14 /* 35 */
#define CborTag_MimeType	15 /* 36 */
#define CborTag_Self		16 /* 55799 */

static inline
uint8_t
cbor_deduce_minor(uintmax_t value)
{
	return value <= CborMin_Higher ? (uint8_t)value
	                      : !(value >> 8) ? CborMin_1_Byte
	                      : !(value >> 16) ? CborMin_2_Bytes
	                      : !(value >> 32) ? CborMin_4_Bytes
	                      : CborMin_8_Bytes;
}

/*****************************************************************************/
/*****************************************************************************/
/**************                                                 **************/
/**************      basics                                     **************/
/**************                                                 **************/
/*****************************************************************************/
/*****************************************************************************/

static
int
make_basic32(
	jcbor_t **j,
	uint8_t major,
	uint8_t minor
) {
	struct _basic32_ *r;

	r = malloc(sizeof *r);
	if (!r) {
		*j = 0;
		return -ENOMEM;
	}

	r->flags = shifted(2, RefCount_Shift)
		 | shifted(major, CborMaj_Shift)
		 | shifted(minor, CborMin_Shift);
	*j = (jcbor_t*)r;
	return 0;
}

static
int
make_basic64(
	jcbor_t **j,
	uint8_t major,
	uint8_t minor
) {
	struct _basic64_ *r;

	r = malloc(sizeof *r);
	if (!r) {
		*j = 0;
		return -ENOMEM;
	}

	r->flags = shifted(2, RefCount_Shift)
		 | shifted(major, CborMaj_Shift)
		 | shifted(minor, CborMin_Shift);
	*j = (jcbor_t*)r;
	return 0;
}

/*****************************************************************************/
/*****************************************************************************/
/**************                                                 **************/
/**************      buffers                                    **************/
/**************                                                 **************/
/*****************************************************************************/
/*****************************************************************************/

#define Mode_Copy	0
#define Mode_Static	1
#define Mode_Acquire	2

#define INCLUDED_LENGTH  (sizeof(union _buffer_) - OFFSETOF(union _buffer_,value))

static
int
make_buffer(
	jcbor_t **j,
	const char *value,
	size_t length,
	uint8_t mode,
	uint8_t major
) {
	uint8_t minor, copy, strfree;
	union _buffer_ *r;
	size_t size;

	/* check length not toooooo biggggg! */
	if (length >= INT_MAX) {
		*j = 0;
		return -EINVAL;
	}

	/* compute the minor value */
	minor = cbor_deduce_minor(length);
	size = length + !(major - CborMaj_String);
	copy = mode == Mode_Copy;
	strfree = mode == Mode_Acquire;

	/* allocate the structure */
	r = malloc(sizeof *r + (copy ? size - INCLUDED_LENGTH : 0));
	if (!r) {
		*j = 0;
		return -ENOMEM;
	}

	/* initialize the structure */
	r->flags = shifted(2, RefCount_Shift)
		 | shifted(strfree, Free_Shift)
		 | shifted(copy, StrVal_Shift)
		 | shifted(major, CborMaj_Shift)
		 | shifted(minor, CborMin_Shift);
	r->length = length;
	if (copy)
		memcpy(r->value, value, size);
	else
		r->cstr = value;
	*j = (jcbor_t*)r;
	return 0;
}

static
inline
bool
is_buffer(
	const jcbor_t *j
) {
	uint8_t maj;
	return j && is_regular(j) && ((maj = get_cbor_major(j->flags)),
			(maj == CborMaj_Binary || maj == CborMaj_String));
}

static
inline
bool
is_binary(
	const jcbor_t *j
) {
	return j && is_regular(j) && get_cbor_major(j->flags) == CborMaj_Binary;
}

static
inline
bool
is_string(
	const jcbor_t *j
) {
	return j && is_regular(j) && get_cbor_major(j->flags) == CborMaj_String;
}

static
inline
size_t
buffer_length(
	const jcbor_t *j
) {
	ASSERT(is_buffer(j));
	return (size_t)j->buffer.length;
}

static
inline
const char *
buffer_value(
	const jcbor_t *j
) {
	ASSERT(is_buffer(j));
	return get_flags(j->flags, StrVal_Mask, StrVal_Shift) ? j->buffer.value : j->buffer.cstr;
}

static
inline
int
buffer_compare(
	const jcbor_t *a,
	const jcbor_t *b
) {
	ASSERT(is_buffer(a));
	ASSERT(is_buffer(b));
	size_t la = a->buffer.length, lb = b->buffer.length;
	return memcmp(buffer_value(a), buffer_value(b), la <= lb ? la : lb) ?: (int)(la - lb);
}

/*************************************************************************/

jcbor_type_t
jcbor_get_type(
	const jcbor_t *j
) {
	/* fake pointers can record values */
	switch (fake_type(j)) {

	case Pointer_Fake_Special:
		switch (fake_value(j)) {
		case CborMin_False:
		case CborMin_True:
			return jcbor_Bool;
		case CborMin_Null:
			return jcbor_Null; /* hum? see below */
		}
		return jcbor_Undefined;

	case Pointer_Fake_Int:
		return jcbor_Int;

	case Pointer_Fake_Float:
		return jcbor_Double;
	}

	/* null pointer is null value */
	if (!j)
		return jcbor_Null;

	/* fake pointers can record values */
	switch (get_cbor_major(j->flags)) {
	case CborMaj_PosInt:
	case CborMaj_NegInt:
		return jcbor_Int;
	case CborMaj_Binary:
		return jcbor_Binary;
	case CborMaj_String:
		return jcbor_String;
	case CborMaj_Array:
		return jcbor_Array;
	case CborMaj_Map:
		return jcbor_Object;
	case CborMaj_Tag:
		return jcbor_Undefined;
	case CborMaj_Simple:
		switch (get_cbor_minor(j->flags)) {
		case CborMin_4_Bytes:
		case CborMin_8_Bytes:
			return jcbor_Double;
		default:
			return jcbor_Undefined;
		}
	}
	return jcbor_Undefined;
}

/*************************************************************************/

jcbor_t *
jcbor_addref(
	const jcbor_t *j
)
{
	jcbor_t *m = (jcbor_t*)j;

	if (m && is_regular(m))
		__atomic_add_fetch(&m->flags, shifted(2, RefCount_Shift), __ATOMIC_RELAXED);
	return m;
}

void
jcbor_unref(
	const jcbor_t *j
)
{
	uintptr_t r;
	jcbor_t *m = (jcbor_t*)j;

	if (m && is_regular(m)) {
		r = __atomic_sub_fetch(&m->flags, shifted(2, RefCount_Shift), __ATOMIC_RELAXED);
		if (!(r >> RefCount_Shift)) {
			switch (get_cbor_major(m->flags)) {
			case CborMaj_Binary:
			case CborMaj_String:
				if (get_flags(m->flags, 1, Free_Shift))
					free(m->buffer.str);
				break;
			case CborMaj_Array:
			case CborMaj_Map:
				break;
			default:
				break;
			}
			free(m);
		}
	}
}

int
jcbor_compare(
	const jcbor_t *a,
	const jcbor_t *b
)
{
	int r;
	jcbor_type_t t;

	if (a == b)
		return 0;

	t = jcbor_get_type(a);
	r = (int)t - (int)jcbor_get_type(b);
	if (r)
		return r;

	switch (t) {
	case jcbor_Null:
		return 0;
	case jcbor_Bool:
		return (int)((intptr_t)a - (intptr_t)b);
	case jcbor_Double:
		return 0;
	case jcbor_Int:
		return 0;
	case jcbor_Object:
		return 0;
	case jcbor_Array:
		return 0;
	case jcbor_String:
	case jcbor_Binary:
		return buffer_compare(a, b);
	case jcbor_Undefined:
		return 0;
	}
}

/*****************************************************************************/

jcbor_t*
jcbor_null(
) {
	return 0;
}

bool
jcbor_is_null(
	jcbor_t *j
) {
	return !j;
}

/*****************************************************************************/

jcbor_t*
jcbor_undefined(
) {
	return fake_special_make(CborMin_Undefined);
}

bool
jcbor_is_undefined(
	jcbor_t *j
) {
	return j == fake_special_make(CborMin_Undefined);
}

/*****************************************************************************/

jcbor_t*
jcbor_false(
) {
	return fake_special_make(CborMin_False);
}

jcbor_t*
jcbor_true(
) {
	return fake_special_make(CborMin_True);
}

jcbor_t*
jcbor_bool(
	int value
) {
	ASSERT((CborMin_True - !value) == (value ? CborMin_True : CborMin_False));
	return fake_special_make(CborMin_True - !value);
}

bool
jcbor_is_bool(
	jcbor_t *j
) {
	return j == fake_special_make(CborMin_False)
	    || j == fake_special_make(CborMin_True);
}

bool
jcbor_bool_value(
	jcbor_t *j
) {
	ASSERT(jcbor_is_bool(j));
	return j == fake_special_make(CborMin_True);
}

/*****************************************************************************/
#if 0
int
jcbor_int32(
	jcbor_t **j,
	int32_t value
) {
	int r;
	if (can_fake_value(value)) {
		*j = fake_int_make(value);
		r = 0;
	} else {
		r = make_basic(j, );
		if (!r)
			(*j)->s32 = 
	}
	return r;
}

bool
jcbor_is_int32(
	jcbor_t *j
) {
	return 
}

int32_t
jcbor_int32_value(
	jcbor_t *j
) {
}

#endif
/*****************************************************************************/

int
jcbor_binary_copy(
	jcbor_t **j,
	const void *value,
	size_t length
) {
	return make_buffer(j, value, length, Mode_Copy, CborMaj_Binary);
}

int
jcbor_binary_static(
	jcbor_t **j,
	const void *value,
	size_t length
) {
	return make_buffer(j, value, length, Mode_Static, CborMaj_Binary);
}

int
jcbor_binary_acquire(
	jcbor_t **j,
	void *value,
	size_t length
) {
	return make_buffer(j, value, length, Mode_Acquire, CborMaj_Binary);
}

bool
jcbor_is_binary(
	const jcbor_t *j
) {
	return is_binary(j);
}

size_t
jcbor_binary_length(
	const jcbor_t *j
) {
	ASSERT(is_binary(j));
	return buffer_length(j);
}

const void *
jcbor_binary_value(
	const jcbor_t *j
) {
	ASSERT(is_binary(j));
	return (const void*)buffer_value(j);
}

/*****************************************************************************/

int
jcbor_string_copy(
	jcbor_t **j,
	const char *value,
	size_t length
) {
	return make_buffer(j, value, length, Mode_Copy, CborMaj_String);
}

int
jcbor_string_static(
	jcbor_t **j,
	const char *value,
	size_t length
) {
	return make_buffer(j, value, length, Mode_Static, CborMaj_String);
}

int
jcbor_string_acquire(
	jcbor_t **j,
	char *value,
	size_t length
) {
	return make_buffer(j, value, length, Mode_Acquire, CborMaj_String);
}

int
jcbor_string_copy_z(
	jcbor_t **j,
	const char *value
) {
	return jcbor_string_copy(j, value, strlen(value));
}

int
jcbor_string_static_z(
	jcbor_t **j,
	const char *value
) {
	return jcbor_string_static(j, value, strlen(value));
}

int
jcbor_string_acquire_z(
	jcbor_t **j,
	char *value
) {
	return jcbor_string_acquire(j, value, strlen(value));
}

bool
jcbor_is_string(
	const jcbor_t *j
) {
	return is_string(j);
}

size_t
jcbor_string_length(
	const jcbor_t *j
) {
	ASSERT(is_string(j));
	return buffer_length(j);
}

const char *
jcbor_string_value(
	const jcbor_t *j
) {
	ASSERT(is_string(j));
	return buffer_value(j);
}

