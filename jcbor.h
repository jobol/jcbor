
#pragma once

#include <stdint.h>
#include <stdbool.h>

/**
 * The types of jcbor_t objects
 */
enum jcbor_type {
	jcbor_Null	= 0,
	jcbor_Bool	= 1,
	jcbor_Double	= 2,
	jcbor_Int	= 3,
	jcbor_Object	= 4,
	jcbor_Array	= 5,
	jcbor_String	= 6,
	jcbor_Binary	= 7,
	jcbor_Undefined	= 8,
};

union jcbor;

typedef union jcbor       jcbor_t;
typedef enum  jcbor_type  jcbor_type_t;

/*************************************************************************/
extern
jcbor_type_t
jcbor_get_type(
	const jcbor_t *j
);

/*************************************************************************/
extern
int
jcbor_compare(
	const jcbor_t *a,
	const jcbor_t *b
);

/*************************************************************************/
extern
jcbor_t *
jcbor_addref(
	const jcbor_t *j
);

extern
void
jcbor_unref(
	const jcbor_t *j
);

/*************************************************************************/
extern
int
jcbor_binary_copy(
	jcbor_t **j,
	const void *value,
	size_t length
);

extern
int
jcbor_binary_static(
	jcbor_t **j,
	const void *value,
	size_t length
);

extern
int
jcbor_binary_acquire(
	jcbor_t **j,
	void *value,
	size_t length
);

extern
bool
jcbor_is_binary(
	const jcbor_t *j
);

extern
size_t
jcbor_binary_length(
	const jcbor_t *j
);

extern
const void *
jcbor_binary_value(
	const jcbor_t *j
);

/*************************************************************************/

extern
int
jcbor_string_copy(
	jcbor_t **j,
	const char *value,
	size_t length
);

extern
int
jcbor_string_static(
	jcbor_t **j,
	const char *value,
	size_t length
);

extern
int
jcbor_string_acquire(
	jcbor_t **j,
	char *value,
	size_t length
);

extern
int
jcbor_string_copy_z(
	jcbor_t **j,
	const char *value
);

extern
int
jcbor_string_static_z(
	jcbor_t **j,
	const char *value
);

extern
int
jcbor_string_acquire_z(
	jcbor_t **j,
	char *value
);

extern
bool
jcbor_is_string(
	const jcbor_t *j
);

extern
size_t
jcbor_string_length(
	const jcbor_t *j
);

extern
const char *
jcbor_string_value(
	const jcbor_t *j
);

/*************************************************************************/
/*****************************************************************************/

extern
jcbor_t*
jcbor_null(
);

extern
bool
jcbor_is_null(
	jcbor_t *j
);

/*****************************************************************************/

extern
jcbor_t*
jcbor_undefined(
);

extern
bool
jcbor_is_undefined(
	jcbor_t *j
);

/*****************************************************************************/

extern
jcbor_t*
jcbor_false(
);

extern
jcbor_t*
jcbor_true(
);

extern
jcbor_t*
jcbor_bool(
	int value
);

extern
bool
jcbor_is_bool(
	jcbor_t *j
);

extern
bool
jcbor_bool_value(
	jcbor_t *j
);

/*****************************************************************************/
